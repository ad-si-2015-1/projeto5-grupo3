var fs = require('fs')
    , http = require('http')
    , socketio = require('socket.io');
    
    global.jogadores = [];
    global.cartas = [];
    var jogador1 = "";
    var aux = 0;
    var j1 = '';
    var j2 = '';
    

var server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-type': 'text/html'});
    res.end(fs.readFileSync(__dirname + '/cliente.html'));
}).listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});

socketio.listen(server).on('connection', function (socket) {

      
      socket.on('iniciarJogo', function (msg){ // Adiciona uma lista dos jogadores
	
	if(global.jogadores.length < 2){ // Isso limita o número de jogadores, que foi projetado para dois jogadores.
	console.log('Jogador: '+ msg);
		global.jogadores.push({'id': socket.id,'nome': msg,'forca': 0, 'agilidade' : 0, 'inteligencia': 0, 'pontos': 0}); // Adiciona o jogador na lista.

		if(global.jogadores.length < 2){ 
		  
		  jogador1 = msg;
		  socket.emit('status', 'Aguardando Jogador...');// Envia uma mensagem de espera para outro jogador conectar
		  
		}else{
		  
		  
		  
		  socket.emit('status', '\nJogador ' + jogador1 + ' está disponível para jogar!');
		  socket.broadcast.emit('status', '\nJogador ' + msg + ' está disponível para jogar!');// Envia o jogador conectado para todos os outros clientes.
		  socket.emit('cartas', 'disponivel');
		  socket.broadcast.emit('cartas', 'disponivel');// Aparecer o botão de iniciar as cartas.
		  /*global.jogadores.forEach( function( valor ) {
			lista += valor.nome+";
		});
		  console.log(lista);*/
		  
		}
	}else{
	  socket.emit('status', 'Infelizmente o servidor está cheio! Tente novamente!');
	}
	
      });

      socket.on('compararCarta', function (msg, msg1, msg2){ // Lógica do jogo.
      
      var lista = '';

	
      
      console.log('Id: '+  socket.id);
      console.log('Força: '+  msg);
      console.log('Agilidade: '+ msg1);
      console.log('Inteligencia: '+ msg2);
      
      global.jogadores.forEach( function( j ) {
		if ( j.id == socket.id ) {
			j.forca = msg;
			j.agilidade = msg1;
			j.inteligencia = msg2;
			aux = aux + 1;
		}
				
	});

     console.log(aux);
     if(aux === 2){
	  
	j2 = socket.id;
	console.log('J1:' +j1);
	console.log('J2:' +j2);
	global.jogadores.forEach( function( j ) {
		if ( j.id == j1 ) {
			global.jogadores.forEach( function( k ) {
			     if ( k.id == j2 ) {
			       console.log('J:' + j.nome);
			       console.log('K: '+ j.nome);
			
				if(j.forca > k.forca){
				  
				    j.pontos++;
				}else{
				  
				    if(j.forca < k.forca){
				   
				      k.pontos++;
				    }
				}
				
				if(j.agilidadea > k.agilidade){
				  
				    j.pontos++;
				}else{
				  
				    if(j.agilidade < k.agilidade){
				   
				      k.pontos++;
				    }
				}
				
				if(j.inteligencia > k.inteligencia){
				  
				    j.pontos++;
				}else{
				  
				    if(j.inteligencia < k.inteligencia){
				   
				      k.pontos++;
				    }
				}
				
				if(j.pontos > k.pontos){
				  
				  socket.emit('vencedor', j.nome);
				  socket.broadcast.emit('vencedor', j.nome);
				  
				}else{
				  
				  if(k.pontos > j.pontos){
				    socket.emit('vencedor', k.nome);
				    socket.broadcast.emit('vencedor', k.nome);
				  }else{
				  
				    socket.emit('vencedor', 'empate');
				    socket.broadcast.emit('vencedor', 'empate');
				  }
				}
			    }
			    
			    
			   
				
	});
			
		}
				
	});
	
	
  
     }else{
	
	   j1 = socket.id;
	   console.log(j1);
     }
});
});
